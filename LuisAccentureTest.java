
// Desafio Técnico_Demandas QA_Accenture

import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNot.not;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import java.util.*;
import java.net.MalformedURLException;
import java.net.URL;
public class LuisAccentureTest {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  @Before
  public void setUp() {
    driver = new ChromeDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  @After
  public void tearDown() {
    driver.quit();
  }
  @Test
  public void luisAccenture() {
    // Test name: LuisAccenture
    // Step # | name | target | value | comment
    // 1 | open | /101/app.php |  | 
    driver.get("http://sampleapp.tricentis.com/101/app.php");
    // 2 | setWindowSize | 1090x628 |  | 
    driver.manage().window().setSize(new Dimension(1090, 628));
    // 3 | click | id=make |  | 
    driver.findElement(By.id("make")).click();
    // 4 | select | id=make | label=Nissan | 
    {
      WebElement dropdown = driver.findElement(By.id("make"));
      dropdown.findElement(By.xpath("//option[. = 'Nissan']")).click();
    }
    // 5 | click | id=make |  | 
    driver.findElement(By.id("make")).click();
    // 6 | click | id=engineperformance |  | 
    driver.findElement(By.id("engineperformance")).click();
    // 7 | type | id=engineperformance | 110 | 
    driver.findElement(By.id("engineperformance")).sendKeys("110");
    // 8 | click | id=dateofmanufacture |  | 
    driver.findElement(By.id("dateofmanufacture")).click();
    // 9 | type | id=dateofmanufacture | 05/20/2021 | 
    driver.findElement(By.id("dateofmanufacture")).sendKeys("05/20/2021");
    // 10 | click | id=numberofseats |  | 
    driver.findElement(By.id("numberofseats")).click();
    // 11 | select | id=numberofseats | label=1 | 
    {
      WebElement dropdown = driver.findElement(By.id("numberofseats"));
      dropdown.findElement(By.xpath("//option[. = '1']")).click();
    }
    // 12 | click | id=numberofseats |  | 
    driver.findElement(By.id("numberofseats")).click();
    // 13 | click | id=fuel |  | 
    driver.findElement(By.id("fuel")).click();
    // 14 | select | id=fuel | label=Petrol | 
    {
      WebElement dropdown = driver.findElement(By.id("fuel"));
      dropdown.findElement(By.xpath("//option[. = 'Petrol']")).click();
    }
    // 15 | click | id=fuel |  | 
    driver.findElement(By.id("fuel")).click();
    // 16 | click | id=listprice |  | 
    driver.findElement(By.id("listprice")).click();
    // 17 | type | id=listprice | 65000 | 
    driver.findElement(By.id("listprice")).sendKeys("65000");
    // 18 | click | id=annualmileage |  | 
    driver.findElement(By.id("annualmileage")).click();
    // 19 | type | id=annualmileage | 11200 | 
    driver.findElement(By.id("annualmileage")).sendKeys("11200");
    // 20 | click | id=nextenterinsurantdata |  | 
    driver.findElement(By.id("nextenterinsurantdata")).click();
    // 21 | click | id=firstname |  | 
    driver.findElement(By.id("firstname")).click();
    // 22 | type | id=firstname | luis | 
    driver.findElement(By.id("firstname")).sendKeys("luis");
    // 23 | type | id=lastname | Fernandez | 
    driver.findElement(By.id("lastname")).sendKeys("Fernandez");
    // 24 | type | id=birthdate | 11/27/1964 | 
    driver.findElement(By.id("birthdate")).sendKeys("11/27/1964");
    // 25 | click | css=.group:nth-child(2) > .ideal-radiocheck-label:nth-child(1) > .ideal-radio |  | 
    driver.findElement(By.cssSelector(".group:nth-child(2) > .ideal-radiocheck-label:nth-child(1) > .ideal-radio")).click();
    // 26 | click | id=country |  | 
    driver.findElement(By.id("country")).click();
    // 27 | select | id=country | label=Brazil | 
    {
      WebElement dropdown = driver.findElement(By.id("country"));
      dropdown.findElement(By.xpath("//option[. = 'Brazil']")).click();
    }
    // 28 | click | id=country |  | 
    driver.findElement(By.id("country")).click();
    // 29 | click | id=zipcode |  | 
    driver.findElement(By.id("zipcode")).click();
    // 30 | type | id=zipcode | 49037490 | 
    driver.findElement(By.id("zipcode")).sendKeys("49037490");
    // 31 | click | id=occupation |  | 
    driver.findElement(By.id("occupation")).click();
    // 32 | select | id=occupation | label=Employee | 
    {
      WebElement dropdown = driver.findElement(By.id("occupation"));
      dropdown.findElement(By.xpath("//option[. = 'Employee']")).click();
    }
    // 33 | click | id=occupation |  | 
    driver.findElement(By.id("occupation")).click();
    // 34 | click | css=.field:nth-child(10) .ideal-radiocheck-label:nth-child(1) > .ideal-check |  | 
    driver.findElement(By.cssSelector(".field:nth-child(10) .ideal-radiocheck-label:nth-child(1) > .ideal-check")).click();
    // 35 | click | id=nextenterproductdata |  | 
    driver.findElement(By.id("nextenterproductdata")).click();
    // 36 | click | id=startdate |  | 
    driver.findElement(By.id("startdate")).click();
    // 37 | type | id=startdate | 10/20/2021 | 
    driver.findElement(By.id("startdate")).sendKeys("10/20/2021");
    // 38 | click | id=insurancesum |  | 
    driver.findElement(By.id("insurancesum")).click();
    // 39 | select | id=insurancesum | label=3.000.000,00 | 
    {
      WebElement dropdown = driver.findElement(By.id("insurancesum"));
      dropdown.findElement(By.xpath("//option[. = '3.000.000,00']")).click();
    }
    // 40 | click | id=insurancesum |  | 
    driver.findElement(By.id("insurancesum")).click();
    // 41 | click | id=meritrating |  | 
    driver.findElement(By.id("meritrating")).click();
    // 42 | select | id=meritrating | label=Bonus 3 | 
    {
      WebElement dropdown = driver.findElement(By.id("meritrating"));
      dropdown.findElement(By.xpath("//option[. = 'Bonus 3']")).click();
    }
    // 43 | click | id=meritrating |  | 
    driver.findElement(By.id("meritrating")).click();
    // 44 | click | id=damageinsurance |  | 
    driver.findElement(By.id("damageinsurance")).click();
    // 45 | select | id=damageinsurance | label=No Coverage | 
    {
      WebElement dropdown = driver.findElement(By.id("damageinsurance"));
      dropdown.findElement(By.xpath("//option[. = 'No Coverage']")).click();
    }
    // 46 | click | id=damageinsurance |  | 
    driver.findElement(By.id("damageinsurance")).click();
    // 47 | click | css=.field:nth-child(5) .ideal-radiocheck-label:nth-child(1) > .ideal-check |  | 
    driver.findElement(By.cssSelector(".field:nth-child(5) .ideal-radiocheck-label:nth-child(1) > .ideal-check")).click();
    // 48 | click | id=courtesycar |  | 
    driver.findElement(By.id("courtesycar")).click();
    // 49 | select | id=courtesycar | label=No | 
    {
      WebElement dropdown = driver.findElement(By.id("courtesycar"));
      dropdown.findElement(By.xpath("//option[. = 'No']")).click();
    }
    // 50 | click | id=courtesycar |  | 
    driver.findElement(By.id("courtesycar")).click();
    // 51 | click | id=nextselectpriceoption |  | 
    driver.findElement(By.id("nextselectpriceoption")).click();
    // 52 | click | css=.choosePrice:nth-child(1) > .ideal-radio |  | 
    driver.findElement(By.cssSelector(".choosePrice:nth-child(1) > .ideal-radio")).click();
    // 53 | click | id=nextsendquote |  | 
    driver.findElement(By.id("nextsendquote")).click();
    // 54 | click | id=email |  | 
    driver.findElement(By.id("email")).click();
    // 55 | click | id=email |  | 
    driver.findElement(By.id("email")).click();
    // 56 | type | id=email | luigcafe@uol.com.br | 
    driver.findElement(By.id("email")).sendKeys("luigcafe@uol.com.br");
    // 57 | click | id=sendQuoteForm |  | 
    driver.findElement(By.id("sendQuoteForm")).click();
    // 58 | click | id=username |  | 
    driver.findElement(By.id("username")).click();
    // 59 | type | id=username | luigcafe | 
    driver.findElement(By.id("username")).sendKeys("luigcafe");
    // 60 | click | id=password |  | 
    driver.findElement(By.id("password")).click();
    // 61 | click | css=.idealsteps-wrap |  | 
    driver.findElement(By.cssSelector(".idealsteps-wrap")).click();
    // 62 | type | id=password | Casa1234 | 
    driver.findElement(By.id("password")).sendKeys("Casa1234");
    // 63 | click | id=confirmpassword |  | 
    driver.findElement(By.id("confirmpassword")).click();
    // 64 | type | id=confirmpassword | Casa@1234 | 
    driver.findElement(By.id("confirmpassword")).sendKeys("Casa@1234");
    // 65 | click | id=password |  | 
    driver.findElement(By.id("password")).click();
    // 66 | click | id=password |  | 
    driver.findElement(By.id("password")).click();
    // 67 | doubleClick | id=password |  | 
    {
      WebElement element = driver.findElement(By.id("password"));
      Actions builder = new Actions(driver);
      builder.doubleClick(element).perform();
    }
    // 68 | click | id=password |  | 
    driver.findElement(By.id("password")).click();
    // 69 | type | id=password | Luis1234 | 
    driver.findElement(By.id("password")).sendKeys("Luis1234");
    // 70 | click | css=.invalid |  | 
    driver.findElement(By.cssSelector(".invalid")).click();
    // 71 | type | id=confirmpassword | Luis1234 | 
    driver.findElement(By.id("confirmpassword")).sendKeys("Luis1234");
    // 72 | click | id=sendemail |  | 
    driver.findElement(By.id("sendemail")).click();
    // 73 | mouseOver | id=sendemail |  | 
    {
      WebElement element = driver.findElement(By.id("sendemail"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element).perform();
    }
    // 74 | mouseOut | id=sendemail |  | 
    {
      WebElement element = driver.findElement(By.tagName("body"));
      Actions builder = new Actions(driver);
      builder.moveToElement(element, 0, 0).perform();
    }
    // 75 | click | css=.confirm |  | 
    driver.findElement(By.cssSelector(".confirm")).click();
  }
}
